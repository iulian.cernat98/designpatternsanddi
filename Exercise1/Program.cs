﻿using Exercise1;

public class EntryPoint
{
    public static void Main(string[] args)
    {
        var builder = new PersonBuilder();
        var person = builder
            .BuildName("Gigi")
            .BuildJobTitle("Developer")
            .BuildWorkHours(8)
            .Build();
        
        Console.WriteLine(person);

    }
}