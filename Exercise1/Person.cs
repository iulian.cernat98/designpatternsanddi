﻿namespace Exercise1;

public class Person
{
    private string _name;
    private string _jobTitle;
    private int _workHours;

    public void SetName(string name)
    {
        _name = name;
    }

    public void SetJobTitle(string jobTitle)
    {
        _jobTitle = jobTitle;
    }

    public void SetWorkHours(int workHours)
    {
        _workHours = workHours;
    }

    public override string ToString()
    {
        return $"Person(Name:{_name}, Job Title: {_jobTitle}, Work Hours : {_workHours})";
    }
}