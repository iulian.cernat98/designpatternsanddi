﻿namespace Exercise1;
    
public interface Builder
{
    Builder BuildName(string name);
    Builder BuildJobTitle(string jobTitle);
    Builder BuildWorkHours(int workHours);
    Person Build();
}