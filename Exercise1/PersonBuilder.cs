﻿namespace Exercise1;

public class PersonBuilder:Builder
{
    private Person _person = new Person();
    public Builder BuildName(string name)
    {
        _person.SetName(name);
        return this;
    }

    public Builder BuildJobTitle(string jobTitle)
    {
        _person.SetJobTitle(jobTitle);
        return this;
    }

    public Builder BuildWorkHours(int workHours)
    {
        _person.SetWorkHours(workHours);
        return this;
    }

    public Person Build()
    {
        return _person;
    }
}