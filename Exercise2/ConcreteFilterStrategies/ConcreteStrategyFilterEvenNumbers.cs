﻿namespace Exercise2;

public class ConcreteStrategyFilterEvenNumbers : IFilterStrategy
{
    public List<int> Filter(List<int> numberList)
    {
        return numberList.Where(number => number % 2 == 0).ToList();
    }
}