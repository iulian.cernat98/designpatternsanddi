﻿namespace Exercise2;

public class ConcreteStrategyFilterOddNumbers : IFilterStrategy
{
    public List<int> Filter(List<int> numberList)
    {
        return numberList.Where(number => number % 2 == 1).ToList();
    }
}