﻿namespace Exercise2;

public class ConcreteStrategyFilterPositiveNumbers : IFilterStrategy
{
    public List<int> Filter(List<int> numberList)
    {
        return numberList.Where(number => number > 10).ToList();
    }
}