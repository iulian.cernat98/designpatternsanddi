﻿namespace Exercise2;

public class ConcreteStrategyFilterMultiplesOf10 : IFilterStrategy
{
    public List<int> Filter(List<int> numberList)
    {
        return numberList.Where(number => number % 10 == 0).ToList();
    }
}