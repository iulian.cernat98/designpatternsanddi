﻿namespace Exercise2;

public class ConcreteStrategyFilterNegativeNumbers : IFilterStrategy
{
    public List<int> Filter(List<int> numberList)
    {
        return numberList.Where(number => number < 0).ToList();
    }
}