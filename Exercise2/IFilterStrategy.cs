﻿namespace Exercise2;

public interface IFilterStrategy
{
    public List<int> Filter(List<int> numberList);
}