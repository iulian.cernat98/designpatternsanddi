﻿namespace Exercise2;

public class FilterableList
{
    private IFilterStrategy _filterStrategy;
    private readonly List<int> _numberList = new();

    public FilterableList(IFilterStrategy defaultFilterStrategy)
    {
        _filterStrategy = defaultFilterStrategy;
    }

    public void SetFilterStrategy(IFilterStrategy newFilterStrategy)
    {
        _filterStrategy = newFilterStrategy;
    }

    public void AddListOfNumbers(List<int> listOfNumbers)
    {
        _numberList.AddRange(listOfNumbers);
    }

    public List<int> FilterListOfNumbers()
    {
        return _filterStrategy.Filter(_numberList);
    }
}