﻿namespace Exercise2;

public static class BusinessLogic
{
    private static readonly int _lowerNumberBound = -100;
    private static readonly int _upperNumberBound = 100;
    private static readonly Random _randomNumberGenerator = new();

    private static List<int> GenerateRandomNumberList(int listSize)
    {
        List<int> numbers = new();
        for (var numberIndex = 0; numberIndex <= listSize; numberIndex++)
            numbers.Add(_randomNumberGenerator.Next(_lowerNumberBound, _upperNumberBound + 1));

        return numbers;
    }

    private static void ShowListOfNumbers(IEnumerable<int> listOfNumbers)
    {
        Console.WriteLine(string.Join(",", listOfNumbers));
    }

    public static void ExecuteBusinessLogic()
    {
        FilterableList listOfNumbers = new(new ConcreteStrategyFilterNegativeNumbers());
        listOfNumbers.AddListOfNumbers(GenerateRandomNumberList(100));

        Console.WriteLine("Negative Numbers");
        ShowListOfNumbers(listOfNumbers.FilterListOfNumbers());

        Console.WriteLine("Positive Numbers");
        listOfNumbers.SetFilterStrategy(new ConcreteStrategyFilterPositiveNumbers());
        ShowListOfNumbers(listOfNumbers.FilterListOfNumbers());

        Console.WriteLine("Even Numbers");
        listOfNumbers.SetFilterStrategy(new ConcreteStrategyFilterEvenNumbers());
        ShowListOfNumbers(listOfNumbers.FilterListOfNumbers());

        Console.WriteLine("Odd numbers");
        listOfNumbers.SetFilterStrategy(new ConcreteStrategyFilterOddNumbers());
        ShowListOfNumbers(listOfNumbers.FilterListOfNumbers());

        Console.WriteLine("Multiples of 10");
        listOfNumbers.SetFilterStrategy(new ConcreteStrategyFilterMultiplesOf10());
        ShowListOfNumbers(listOfNumbers.FilterListOfNumbers());
    }
}